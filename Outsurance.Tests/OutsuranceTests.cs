﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Outsurance.Model;


namespace Outsurance.Tests
{
    [TestClass]
    public class OutsuranceTests
    {
        private const string ExcelfilePath = "C:\\";

        [TestMethod]
        [TestCategory("Outsurance/Validation")]
        public void IsFileCsVwithInvalidFile()
        {
            var fullpath = string.Concat(ExcelfilePath, "test.xlsx");
            var result = Program.IsValidCsvFile(fullpath);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        [TestCategory("Outsurance/Validation")]
        public void IsFileCsVwithValidFile()
        {
            var fullpath = string.Concat(ExcelfilePath, "test.csv");
            var result = Program.IsValidCsvFile(fullpath);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        [TestCategory("Outsurance/Operations")]
        public void WriteToTextFileWithValidData()
        {
            var fullpath = string.Concat(ExcelfilePath, "test.txt");
            var result = Program.WriteToTextFile("blah blah blah", fullpath);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        [TestCategory("Outsurance/Operations")]
        public void WriteToTextFileWithNoFileContent()
        {
            var fullpath = string.Concat(ExcelfilePath, "test.txt");
            var result = Program.WriteToTextFile("", fullpath);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        [TestCategory("Outsurance/Operations")]
        public void WriteToTextFileWithNoFilePath()
        {
            var result = Program.WriteToTextFile("blah blah blah", "");
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        [TestCategory("Outsurance/AddressSorting")]
        public void TestSortAddressWithWrongList()
        {
            var expectedAddresses = new List<string>
            {
                "65 Ambling Way","49 Sutherland St", "8 Crimson Rd", "12 Howard St", "102 Long Lane",
                "94 Roland St", "78 Short Lane","82 Stewart St"
            };
            var result = Program.SortAddress(CsvAddressData()).ToList();
            Assert.IsFalse(result.SequenceEqual(expectedAddresses));
        }

        [TestMethod]
        [TestCategory("Outsurance/AddressSorting")]
        public void SortAddressWithCorrectList()
        {
            var expectedAddresses = new List<string>
            {
                "65 Ambling Way", "8 Crimson Rd", "12 Howard St", "102 Long Lane",
                "94 Roland St", "78 Short Lane","82 Stewart St","49 Sutherland St"
            };
            var result = Program.SortAddress(CsvAddressData()).ToList();
            Assert.IsTrue(result.SequenceEqual(expectedAddresses));
        }

        [TestMethod]
        [TestCategory("Outsurance/SortingName")]
        public void GroupUserNamesWithCorrectNames()
        {
            var expected = new List<NameViewModel>
            {
                new NameViewModel {Name = "Brown",Frequency = 2},
                new NameViewModel {Name = "Clive",Frequency = 2},
                new NameViewModel {Name = "Graham",Frequency = 2},
                new NameViewModel {Name = "Howe",Frequency = 2},
                new NameViewModel {Name = "James",Frequency = 2},
                new NameViewModel {Name = "Owen",Frequency = 2},
                new NameViewModel {Name = "Smith",Frequency = 2},
                new NameViewModel {Name = "Jimmy",Frequency = 1},
                new NameViewModel {Name = "John",Frequency = 1}
            };

            var actual = Program.GenerateNameViewModels(GetNamesDictionary());
            CompareIEnumerable(actual, expected, (x,y)=>x.Name==y.Name&& x.Frequency==y.Frequency);
        }

        protected static IEnumerable<DataViewModel> CsvAddressData()
        {
            return new List<DataViewModel>()
            {
                new DataViewModel {Address = "102 Long Lane"},
                new DataViewModel {Address = "65 Ambling Way"},
                new DataViewModel {Address = "82 Stewart St"},
                new DataViewModel {Address = "12 Howard St"},
                new DataViewModel {Address = "78 Short Lane"},
                new DataViewModel {Address = "49 Sutherland St"},
                new DataViewModel {Address = "8 Crimson Rd"},
                new DataViewModel {Address = "94 Roland St"}
            };




        }

        protected static Dictionary<string, int> GetNamesDictionary()
        {
            var dictionary = new Dictionary<string, int>
            {
                {"Brown", 2},
                {"Clive",2},
                {"Graham",2},
                {"Howe",2},
                {"James",2},
                {"Owen",2},
                {"Smith",2},
                {"Jimmy",1},
                {"John",1}
            };
            return dictionary;
        }

        protected static void CompareIEnumerable<T>(IEnumerable<T> one, IEnumerable<T> two, Func<T, T, bool> comparisonFunction)
        {
            var oneArray = one as T[] ?? one.ToArray();
            var twoArray = two as T[] ?? two.ToArray();

            if (oneArray.Length != twoArray.Length)
            {
                Assert.Fail("Name collections are not same length");
            }

            for (var i = 0; i < oneArray.Length; i++)
            {
                var isEqual = comparisonFunction(oneArray[i], twoArray[i]);
                Assert.IsTrue(isEqual);
            }
        }
    }
}
