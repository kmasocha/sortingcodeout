Instructions for running the application:

1. Open Outsurance project
2. Open App.config file.
3. Change the appsettings values to point to where your excel is and also where you want the text files to be created,i.e. rename the below files:
	<add key="excelFilePath" value="C:\Users\kmasocha\Documents\Projects\Outsurance\data.csv" />
    <add key="addressFilePath" value="C:\Users\kmasocha\Documents\Projects\Outsurance\address.txt" />
    <add key="nameFilePath" value="C:\Users\kmasocha\Documents\Projects\Outsurance\names.txt" />
4. If you have not done so, copy and paste your csv file into the excelFilePath value.
5. You are all set up- now run the console application.

NOTE: Do not change the key name as this will affect the program.

Credits:
Programme written by Kurai Masocha | kmasocha@gmail.com | 083 278 1216
