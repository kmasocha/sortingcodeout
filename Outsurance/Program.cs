﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using Outsurance.Model;

namespace Outsurance
{
   public class Program
    {
        static void Main(string[] args)
        {
            var excelFilePath = ConfigurationManager.AppSettings["excelFilePath"];
            var addressFilePath = ConfigurationManager.AppSettings["addressFilePath"];
            var nameFilePath = ConfigurationManager.AppSettings["nameFilePath"];
            Console.WriteLine("Welcome to Outsuranca Sorting Application done by Kurai Masocha- 083 278 1216");
            Console.WriteLine("If you have read the readme file, press any key to continue....");
            Console.ReadKey();
            Console.WriteLine("Reading csv file");
            if (!IsValidCsvFile(excelFilePath))
            {
                Console.WriteLine("Invalid CSV File");
                Console.ReadKey();
                return;
            }
            var excelData=ReadExcel(excelFilePath);
            if (excelData.Count==0)
            {
                Console.WriteLine("Failed to read csv data");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Excel Data Successfully read, Now sorting names");
            var allNames=ExtractNameAndLastName(excelData);
            if (allNames.Count==0)
            {
                Console.WriteLine("Error in extracting the names list. Please contact the Administrator");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("First Name and Last Name list successfully extracted. Now computing the names");
            var namesDictionary=ComputeDictionary(allNames);
            if (namesDictionary.Count==0)
            {
                Console.WriteLine("Error in computing the names dictionary. Please contact the Administrator");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Successfully computed Dictionary. Now generating view model");
            var nameViewModel = GenerateNameViewModels(namesDictionary);
            if (nameViewModel.Count == 0)
            {
                Console.WriteLine("Error in computing the names dictionary. Please contact the Administrator");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Successfully created view model. Now creating a text file");
            var writingFiles= CreateNames(nameViewModel);
            if (!writingFiles)
            {
                Console.WriteLine("Error in writing text file. Please contact the Administrator");
                Console.ReadKey();
                return;
            }
            var addressSorted=SortAddress(excelData);
            var addressesGenerated=GenerateAddressString(addressSorted);
            if (!addressesGenerated)
            {
                Console.WriteLine("Error in sorting address details. Please contact the Administrator");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Successfully created the notepad files");
            Console.WriteLine("..................................................................");
            Console.WriteLine("Address file can be accessed from:"+ addressFilePath);
            Console.WriteLine("Names file can be accessed from:"+ nameFilePath);
            Console.WriteLine("..................................................................");
            Console.WriteLine("Thank you for reading all the messages to the end.");
            Console.ReadLine();
           
        }

        public static bool IsValidCsvFile(string filePath)
        {
            return (string.IsNullOrWhiteSpace(filePath)||Path.GetExtension(filePath)!=".csv")?false: true;
        }
        private static List<DataViewModel> ReadExcel(string filePath)
        {
            var xlApp = new Application();
            Console.WriteLine("Opening excel");
            var xlWorkbook = xlApp.Workbooks.Open(filePath);
            _Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            var xlRange = xlWorksheet.UsedRange;
            var csvData = new List<DataViewModel>();
            Console.WriteLine("Successfully opened excel");
            try
            {
                var rowCount = xlRange.Rows.Count;
                for (var i = 2; i <= rowCount; i++)
                {
                    var data = new DataViewModel
                    {
                        FirstName = xlRange.Cells[i, 1].Value.ToString(),
                        LastName = xlRange.Cells[i, 2].Value.ToString(),
                        Address = xlRange.Cells[i, 3].Value.ToString(),
                        PhoneNumber = xlRange.Cells[i, 4].Value.ToString()
                    };
                    csvData.Add(data);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("An Error has occured during document processing. Contact the developer :)");
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);

               
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
            }

            return csvData;
        }

        public static List<string> SortAddress(IEnumerable<DataViewModel> dataViewModels)
        {
           return
                dataViewModels.Select(a => a.Address)
                    .OrderBy(o => (Regex.Replace(o, @"[\d-]", string.Empty).TrimStart()))
                    .ToList();
        }

       private static bool GenerateAddressString(List<string> address)
       {
           var buffer = new StringBuilder();
           address.ForEach(item => buffer.AppendLine(String.Format("{0}", item)));
           var addressFilePath = ConfigurationManager.AppSettings["addressFilePath"];
           return WriteToTextFile(buffer.ToString(), addressFilePath);
       }

        public static bool WriteToTextFile(string content,string textFilePath)
        {
            if (string.IsNullOrWhiteSpace(content)|| string.IsNullOrWhiteSpace(textFilePath))
            {
                return false;
            }
            
            if (!File.Exists(textFilePath))
            {
                File.Create(textFilePath);
            }
            try
            {
                File.WriteAllText(textFilePath, content);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
                return false; 
            }
           
        }

        private static List<string> ExtractNameAndLastName(IEnumerable<DataViewModel> dataViewModels)
        {
            var firstNames = dataViewModels.Select(x => x.FirstName.ToString()).ToList();
            var lastNames = dataViewModels.Select(x => x.LastName.ToString()).ToList();

            return firstNames.Concat(lastNames).ToList();
        }

        private static bool CreateNames(IEnumerable<NameViewModel> nameViewModels)
        {
            var buffer = new StringBuilder();
            nameViewModels.ToList().ForEach(item => buffer.AppendLine(String.Format("{0},{1}", item.Name,item.Frequency)));
            var namesFilePath = ConfigurationManager.AppSettings["nameFilePath"];
           return WriteToTextFile(buffer.ToString(), namesFilePath);
        }

        private static Dictionary<string, int> ComputeDictionary(IList<string> allNames)
        {
            var dictionary = new Dictionary<string, int>();
            foreach (var firstName in allNames)
            {
                var pos = allNames.IndexOf(firstName);
                var val = allNames[pos];
                if (dictionary.ContainsKey(val))
                {
                    dictionary[val] = dictionary[val] + 1;
                }
                else
                {
                    dictionary.Add(val, 1);
                }
            }
         return dictionary;
        }

        public static List<NameViewModel> GenerateNameViewModels(Dictionary<string, int> dictionary)
        {
           return dictionary.Select(x => new NameViewModel
            {
                Name = x.Key,
                Frequency = x.Value
            }).OrderByDescending(od => od.Frequency).ThenBy(oa => oa.Name).ToList();
        } 

       

    }
}
