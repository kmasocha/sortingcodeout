﻿

namespace Outsurance.Model
{
   public class NameViewModel
    {
        public string Name { get; set; }
        public int Frequency { get; set; }
    }
}
